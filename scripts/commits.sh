#!/usr/bin/env sh

# Usage: ./scripts/commits.sh content data/commits

set -e

echo "generating git logs of each *.md file for changelogs."

mkdir -p $2

find $1 -name "*.md" | while read line; do
    contentline=${line#"$1/"};
    filename=$(echo -n $contentline | md5sum | cut -f 1 -d " ");
    log=$(git log --pretty=format:'{%n  "commit": "%H",%n  "author": "%aN",%n  "commit_date": "%ci",%n  "message": "%s"%n},' "$line" | perl -pe 'BEGIN{print "["}; END{print "]\n"}' | perl -pe 's/},]/}]/');
     echo $log > "$2/$filename.json";
done


# workaround for commits with invalid characters
rm -f data/commits/8cb8eade5f2a2572d6df34091c9c7d12.json
rm -f data/commits/932529a948a5717a9894cf3b4e361a69.json
rm -f data/commits/7515876eddd2f2a625cb79de7874eea7.json