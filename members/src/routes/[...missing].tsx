import { HttpStatusCode } from "@solidjs/start";

const MissingPage = () => {
  <HttpStatusCode code={404} />;
  return <span>Missing page!</span>;
};

export default MissingPage;
