import { createEffect, createSignal } from "solid-js";
import "./app.css";

const [count, setCount] = createSignal(0);

const up = async (old: number) => {
  "use server";
  await new Promise((r) => setTimeout(r, 2000));
  return old + 1;
};

export default function App() {
  const [busy, setBusy] = createSignal(false);
  return (
    <main>
      <h1>Hello world!</h1>
      <button
        disabled={busy()}
        class="increment"
        onClick={async () => {
          setBusy(true);
          setCount(await up(count()));
          setBusy(false);
        }}
      >
        Clicks: {count()}
      </button>
      <p>
        Visit{" "}
        <a href="https://start.solidjs.com" target="_blank">
          start.solidjs.com
        </a>{" "}
        to learn how to build SolidStart apps.
      </p>
    </main>
  );
}
