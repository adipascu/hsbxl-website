import { Suspense, createResource } from "solid-js";

const parsePam = (data: unknown) => {
  if (!Array.isArray(data)) {
    throw new Error("data is not an array");
  }
  return data.map((it) => {
    if (typeof it !== "string") {
      throw "Value is not  string";
    }
    return it;
  });
};

const fetchPam = async () => {

  await new Promise((r) => {
    setTimeout(r, 4000);
  });

  console.log("FETCH");
  debugger;
  const res = await fetch("https://pam.hsbxl.be/data.php");
  return parsePam(await res.json());
};

const Pam = () => {
  const [user] = createResource(fetchPam);

  return (
    <div>
      <h1>User Details</h1>
      {<pre>{JSON.stringify(user(), null, "\t")}</pre>}
    </div>
  );
};

export default Pam;
