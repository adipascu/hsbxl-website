import { Suspense, createResource } from "solid-js";
import { Route, Router, useParams } from "@solidjs/router";
import { FileRoutes } from "@solidjs/start/router";

const Fallback = () => {
  const { path } = useParams<{ path: string }>();
  const [data] = createResource(async () => {
    const res = await fetch("https://hsbxl.be/" + path + "/");
    return await res.text();
  });
  return data()
};
export default function App() {
  return (
    <Router root={(props) => <Suspense>{props.children}</Suspense>}>
      <FileRoutes />
      {/* <Route path="*path" component={Fallback} /> */}
    </Router>
  );
}
