const login = async (username: string, password: string) => {
  const url = "https://tools.hsbxl.be/ldapviewer.php";
  const params = new URLSearchParams();
  params.append("user", username);
  params.append("pass", password);
  params.append("dataObject", "Padlock codes");

  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: params.toString(),
    });

    const data = await response.text();
    if (response.ok) {
      if (
        data.includes("Logged in as adipascu") ||
        data.includes("Logged is as adipascu")
      ) {
        return "AuthSuccess";
      } else if (data.includes("LDAP failed: Invalid credentials")) {
        return "AuthFail";
      } else {
        return "UnknownError";
      }
    } else {
      return "UnknownError";
    }
  } catch (error) {
    console.error("Error:", error);
    return "UnknownError";
  }
};

login("adipascu", "xxx").then(console.log);
