import { defineConfig } from "@solidjs/start/config";

export default defineConfig({
  server: {
    preset: "vercel",
    routeRules: {
      // "/new": { redirect: "/aas" },
      // "/old/**": { proxy: "https://hsbxl.be/**" },
      // "/old/**": { proxy: "http://localhost:56991/**" },
    },
    prerender: {
      crawlLinks: true,
      failOnError: true,
    },
  },
});
