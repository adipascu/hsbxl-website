---
techtuenr: "651"
startdate: 2022-10-18
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue650
series: TechTuesday
title: TechTuesday 651
linktitle: "TechTue 651"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
