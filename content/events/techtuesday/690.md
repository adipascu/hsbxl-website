---
techtuenr: "690"
startdate: 2023-07-18
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue690
series: TechTuesday
title: TechTuesday 690
linktitle: "TechTue 690"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
