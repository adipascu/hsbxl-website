---
startdate:  2019-10-12
starttime: "14:30"
endtime: "17:00"
linktitle: "Tech met kids"
title: "Tech met kids"
price: "Free"
image: "tech_with_kids"
series: "Tech With Kids"
eventtype: "for kids age 6+"
location: "HSBXL"
---

Tech With Kids is a get together of kids with parents to work on projects, learn new things, play with tech.
